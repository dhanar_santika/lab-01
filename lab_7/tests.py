from django.test import TestCase,Client
from .models import Friend
from .views import add_friend,model_to_dict,delete_friend,validate_npm
from lab_7.api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_add_friend(self):
        request = Client().post("/lab-7/add-friend/",data={'name':'Test','npm':1606880996})
        counting_all_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_friend,1)

    def test_lab_7_delete_friend(self):
        friend = Friend.objects.create(friend_name="Test",npm=1606880996)
        friend.save()
        id = Friend.objects.all().count()
        delete_friend(Friend,id)
        counting_all_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_friend,0)

    def test_lab_7_validate_npm(self):
        friend = Friend.objects.create(friend_name="Test",npm=1606880996)
        friend.save()
        request = Client().post('/lab-7/validate-npm/',data={'npm':friend.npm})
        self.assertEqual(request.status_code, 200)

    def test_lab_7_friend_return_str(self):
        friend = Friend.objects.create(friend_name="Test",npm=1606880996)
        self.assertEqual(friend.__str__(),"Test")

    def test_lab_7_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        list_friend = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab_7_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'],csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_lab_7_raise_invalid_sso_exception(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.username = "anang"
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token()
        self.assertIn("username atau password sso salah", str(context.exception))