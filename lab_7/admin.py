from django.contrib import admin
from .models import Friend

# Register your models here.

class FriendAdmin(admin.ModelAdmin):
    prepopulated_fields = {'npm': ('friend_name',)}

admin.site.register(Friend)