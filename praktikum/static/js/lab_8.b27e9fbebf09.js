  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '376622882777109',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
     $('#lab8').html("<div align='center'><button onclick='facebookLogin()' class='btn btn-primary'>Login with Facebook</button></div><div id='lab8-user'></div><div id='lab8-post'></div>");
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.

      $('#lab8').html("<div id='lab8-user'></div><div class='col-md-6' align='left'><div id='lab8-post'></div></div>")
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8-user').html(
          '<div class="profile">' +
            '<div align="center"><img class="cover" src="' + user.cover.source + '" alt="cover" /></div>' +
            '<div class="col-md-4 user-info"><img class="picture img-thumbnail" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h3>' + '<span class="glyphicon glyphicon-user"></span> About: ' + user.about + '</h3>' +
              '<h3>' + '<span class="glyphicon glyphicon-envelope"></span> Email: ' + user.email + ' </h3> ' + 
              '<h3>' + '<span class="fa fa-venus-mars"></span> Sex: ' + user.gender + ' </h3> ' + 
            '</div>' +
            '<button class="logout btn btn-primary" style="background-color:red" onclick="facebookLogout()">Logout</button></div>' +                      
          '</div>' +
          '<div class="col-md-6 user-status"><div class="form-group">' +
          '<label for="postInput">Status:</label>' +
          '<textarea id="postInput" type="text" class="post form-control" rows="5" placeholder="Ketik Status Anda" />' +
          '</div>' +
          '<button class="postStatus btn btn-primary" onclick="postStatus()">Post ke Facebook</button></div>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8-post').append(
                '<div class="feed col-md-8">' +
                '<div style="display:inline-block" align="left">'+
                  '<h4>' + value.message +'</h4>' +
                  '<h5>' + value.story + '</h5>' +
                '</div>' +
                '<div style="display:inline-block;float:right;" align="right">'+
                  '<button onclick="deleteStatus('+"\'"+value.id+"\'"+')" class="btn btn-primary" style="background-color:red">X</button>'+
                '</div>'+
                '</div>'
              );
            } else if (value.message) {
              $('#lab8-post').append(
                '<div class="feed col-md-8">' +
                '<div style="display:inline-block" align="left">'+
                  '<h4>' + value.message +'</h4>' +
                '</div>' +
                '<div style="display:inline-block;float:right;" align="right">'+
                '<button onclick="deleteStatus('+"\'"+value.id+"\'"+')" class="btn btn-primary" style="background-color:red">X</button>'+
                '</div>'+
                '</div>'
              );
            } else if (value.story) {
              $('#lab8-post').append(
                '<div class="feed col-md-8">' +
                '<div style="display:inline-block" align="left">'+
                  '<h5>' + value.story + '</h5>' +
                '</div>' +
                '<div style="display:inline-block;float:right;" align="right">'+
                '<button onclick="deleteStatus('+"\'"+value.id+"\'"+')" class="btn btn-primary" style="background-color:red">X</button>'+
                '</div>'+
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html("<div align='center'><button onclick='facebookLogin()' class='btn btn-primary'>Login with Facebook</button></div><div id='lab8-user'></div><div class='col-md-6' align='left'><div id='lab8-post'></div></div>");
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
        console.log(response);
        render(response.status=="connected");
      }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'}) 
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);          
        }
     });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.api('/me?fields=id,name,about,email,gender,cover,picture.type(large)', 'GET', function (response){
      fun(response);
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.api('/me/feed', 'GET', function (response){
        fun(response);
    });
  };

  const postFeed = (message_status) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    var message = message_status;
    FB.api('/me/feed', 'POST', {message:message}, function(response){
      $('#lab8-post').prepend(
        '<div class="feed col-md-6">' +
        '<div style="display:inline-block" align="left">'+
          '<h4>' + message +'</h4>' +
        '</div>' +
        '<div style="display:inline-block;float:right;" align="right">'+
        '<button onclick="deleteStatus('+"\'"+response.id+"\'"+')" class="btn btn-primary" style="background-color:red">X</button>'+
        '</div>'+
        '</div>'
        );
      $('#postInput').val("");
    });
  };

  const deleteStatus = (id) => {
    // Delete feed dari apps ke fb
    FB.api("/"+id , "DELETE" , function(response){console.log(id)});
  }

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
